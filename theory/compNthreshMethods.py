import numpy as np
import matplotlib.pyplot as plt



# Chain
LP = 5.
reffs = np.linspace(1,50)
Nthreshes_1 = 0*reffs
Nthreshes_2 = 0*reffs
for i in range(len(reffs)):
    reff = reffs[i]

    # Easy method:

    # N_K = N sigma / (2*L_P)
    # RG ~ 2LP * N_K^0.588 / sqrt(6)
    # reff = RG(Nthresh)
    # NKthresh = (sqrt(6)*reff/2LP)^(1/0.588)
    # Nthresh = 2*LP*NKthresh
    Nthresh_1 = 2*LP*((6**0.5)*reff/(2*LP))**(1./0.588)
    Nthreshes_1[i] = Nthresh_1

    # Harder method:

    # RG^2 = (1/3) LP LC - LP^2 + 2 (LP^3/LC) * ( 1 - (LP/LC) (1 - exp(-LC/LP)))
    # thus, need to solve for N in
    # reff^2 = (LP/3) N - LP^2 + 2LP^3 (1/N) ( 1 - (LP/N) (1 - exp(-N/LP)))
    # rewrite as
    # N = (1/2LP^3) ( (LP/3)N^3 - (LP^2 + reff^2) N^2 )  +  LP (1 - exp(-N/LP))
    # Attempt fixed-point iteration, using Nthresh_1 as guess

    Nthresh_2 = Nthresh_1
    Nold = Nthresh_2
    err = 1
    itn = 0
    while ( err > 1e-6 ):
        itn+=1
        
        Nnew = -(1/(2*LP**3)) * ( (LP/3.)*Nold**3 - (LP**2 + reff**2)*Nold**2 ) + LP * ( 1 - np.exp(-Nold/LP) )
        factor = (LP/Nthresh_1)**2
        Nnew = factor*Nnew + (1-factor)*Nold
        err = np.abs( (Nnew-Nold)/Nnew )
        Nold = Nnew

        #    plt.plot(itn,Nnew,'*',markersize=5)

    Nthreshes_2[i] = Nnew

plt.plot(reffs,Nthreshes_1,lw=3,label='Naive - Inv')
plt.plot(reffs,Nthreshes_2,lw=3,label='Full - Inv')



# Sanity check: Plot RG vs N
Ns = np.linspace(50,500,1000)

# RG ~ 2LP * N_K^0.588 / sqrt(6)
NKs = Ns / (2*LP)
RG_1 = 2 * LP * NKs**0.588 / np.sqrt(6)

plt.plot(RG_1,1.5*Ns,'k--',lw=3,label='Naive - Direct')


# RG = (1/3) LP LC - LP^2 + 2 (LP^3/LC) * ( 1 - (LP/LC) (1 - exp(-LC/LP))) 
LCs = Ns # approx
RG_2 = np.sqrt( 
    (LP/3.) * LCs - LP**2 + 2 * (LP**3/LCs) * ( 1 - (LP/LCs) * (1 - np.exp(-LCs/LP))) )

plt.plot(RG_2,Ns,'k--',lw=3,label='Naive - Direct')


# Check inversion worked correctly
plt.legend()
plt.show()
#plt.clf()



# # The Theory:
# #  The naive formula will work with a different constant of proportionality
# #  Find that here
# for k in np.linspace(1,3,5):

#     RG_1 = k * NKs**0.588
#     plt.plot(Ns,RG_1,lw=2,label='k = %f'%k)

# plt.plot(Ns,RG_2,lw=3,label='Full')
# plt.legend()
# plt.show()

