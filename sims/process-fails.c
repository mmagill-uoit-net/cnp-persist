#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


// Reads in output for a case from the data folder
// Each run has a pair of files, one _states and one _fail
// This file processes the _fail files

// Program Inputs
// Takes in the _fail.dat filename of the case.

// Case Inputs
// Example filename: 150_1._3.5_1._0.8_50_1503123456_states.dat
// N = 150
// reffcis   = 1.0
// reffcavi  = 3.5
// refftrans = 1.0
// Flux = 0.8
// nevents = 50
// rseed = 1503123456

// Case Outputs
// n_fail:     	Number of failed events
// t_fail:     	Average time to fail for failed events


int main (int argc, char* argv[]) {

  // Parse input: file should be of the form data/[...].dat
  if ( argc != 2 ) { printf("%s data/[_states filename]\n",argv[0]); return 1; }
  char* filename_fail = argv[1]; // Copy this for later
  int N; float reffcis; float reffcavi; float refftrans; float flux; int nevents; long int rseed;
  sscanf(filename_fail,"data/%d_%f_%f_%f_%f_%d_%ld_states.dat",&N,&reffcis,&reffcavi,&refftrans,&flux,&nevents,&rseed);

  // Input files
  FILE* ffail = fopen(filename_fail,"r"); //printf("%s\n",filename_fail); 

  // Output files
  char* filename_output = "metrics_fails.dat"; FILE* fout_fail = fopen(filename_output,"a");

  /////////////////////////////////////////////////////////////////////////////////////////////////

  //// MAIN LOOP

  char line[256];
  int failtime; // Tmp variable for fail time of each failure
  int nfail=0; // Total number of fails
  float tfail=0; // Average fail time

  // Loop over lines in states file
  while (fgets(line,256,ffail)) {

    // Scan next line for the event
    sscanf(line,"%d\n",&failtime);
    nfail++;
    tfail = tfail + failtime;
  }

  // Finish metric calculations
  if ( nfail > 0 ) { tfail = tfail / (float) nfail; }
  else { tfail = 0; }
  // Output metrics
  fprintf(fout_fail,"%d %f %f %f %f %d %ld %d %f\n",
	  N,reffcis,reffcavi,refftrans,flux,nevents,rseed,
	  nfail,tfail);


  return 0;
}
