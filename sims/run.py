#!/usr/bin/python

import numpy as np
import subprocess as prc

# Global parameters
myqueue = 'serial'
myespresso = '/home/magill/espresso-2.1.2j-harmonic-kraken/Espresso'
myinputscript = 'cnp-persist.tcl'
rseed_suffix = 123456
vis_flag = 0
#nevents = 100
nevents = 50 # Coarse search for now

# First off, reproduce the old results
reff_cis = 1.
reff_trans = reff_cis


# Main loop
for rseed_prefix in np.arange(10)+1:

    for flux in [0.4,0.5,0.6,0.7,1.0,1.5]:

        for reff_cavi in [3.0,3.5,4.0]:

            for N in (np.arange(7)+1)*20:

                # Run time
                mytime_in_mins = 5000

                # Same for all cases
                rseed = int(str(N) + str(rseed_prefix) + str(rseed_suffix))
                mylabel = (str(N) +'_'+ str(reff_cis) +'_'+ str(reff_cavi) +'_'+ 
                           str(reff_trans) +'_'+ str(flux) +'_'+ str(rseed))
                prc.check_output(['sqsub',
                                  '-q', myqueue,
                                  '-o', 'logs/log_%s.log' % mylabel,
                                  '-r', str(mytime_in_mins)+'m',
                                  myespresso, myinputscript,
                                  str(N), 
                                  str(reff_cis), str(reff_cavi), str(reff_trans),
                                  str(flux), str(vis_flag), str(nevents),
                                  str(rseed)])
        



