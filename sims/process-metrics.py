import numpy as np


# Dictionary
ind_N=0
ind_rcis=1
ind_rcavi=2
ind_rtrans=3
ind_flux=4
ind_nevents=5
ind_rseed=6
ind_ttrans=7
ind_tlastthread=8
ind_texit=9
ind_nexit=10
ind_nthresh=11
ind_tfill=12
ind_tstuck=13
ind_nstuck=14
ind_ntail=15

ind_nevents=5
ind_nfail=7
ind_tfail=8


# Take the metrics_states and metrics_fails files
#  and compute final averages for each (N,rcavi,F)

Astates = np.loadtxt('metrics_states.dat')
Afails = np.loadtxt('metrics_fails.dat')

### Need to remove straight-through events
### Maybe make this a variable as a function of (N,r,F)

# Identify (N,r,F) list
rcavis = np.unique(Astates[:,ind_rcavi])
fluxes = np.unique(Astates[:,ind_flux])
Output = np.zeros((0,24))


# Find indices of rows corresponding to (N,r,F) cases
def Filter_partial(A,rcavi,flux):
    return np.where( ( abs(A[:,ind_rcavi]-rcavi)<0.01 ) & 
                     ( abs(A[:,ind_flux]-flux)<0.01 ) )

def Filter(A,N,rcavi,flux):
    return np.where( ( abs(A[:,ind_N]-N)<0.01 ) & 
                     ( abs(A[:,ind_rcavi]-rcavi)<0.01 ) & 
                     ( abs(A[:,ind_flux]-flux)<0.01 ) )


i=0
# Ns is not the same for every (r,F)
for rcavi in rcavis:
    for flux in fluxes:
        Ns = np.unique(Astates[Filter_partial(Astates,rcavi,flux),ind_N])
        for N in Ns:

            # Select the current (N,r,F)
            Bstates = Astates[Filter(Astates,N,rcavi,flux)]
            Bfails  =  Afails[Filter(Afails,N,rcavi,flux)]

            ## Filter out straight-through (ST) events
            Bstates_ST = Astates[np.where((Bstates[:,ind_tfill]<=0)|
                                          (Bstates[:,ind_tstuck]<=0))]
            STrate = float(np.shape(Bstates_ST)[0]) / float(np.shape(Astates)[0])
            print "%.3f %%" % (100*STrate)
            Bstates = Bstates[np.where((Bstates[:,ind_tfill]>0)&
                                       (Bstates[:,ind_tstuck]>0))]

            ## Means
            # Translocation time
            mean_ttrans = np.mean(Bstates[:,ind_ttrans])
            # Last Thread time
            mean_tlastthread = np.mean(Bstates[:,ind_tlastthread])
            # Exit time and count
            mean_texit = np.mean(Bstates[:,ind_texit])
            mean_nexit = np.mean(Bstates[:,ind_nexit])
            # Fill time
            mean_tfill = np.mean(Bstates[:,ind_tfill])
            # Stuck time and count, tail count
            mean_tstuck = np.mean(Bstates[:,ind_tstuck])
            mean_nstuck = np.average(Bstates[:,ind_nstuck],weights=Bstates[:,ind_tstuck])
            mean_ntail = np.average(Bstates[:,ind_ntail],weights=Bstates[:,ind_tstuck])
            # Fail rate
            Nsuccesses = np.shape(Bstates)[0]
            Nfails = np.sum(Bfails[:,ind_nfail])
            failrate = Nfails / (Nsuccesses + Nfails)
            # Average time to fail for failed events (already partially averaged)
            #mean_tfail = np.average(Bfails[:,6],weights=Bfails[:,5]) # Doesn't work when weights are all zero
            mean_tfail = np.average(Bfails[:,ind_nfail]*Bfails[:,ind_tfail])

            ## Standard Deviations
            # Translocation time
            std_ttrans = np.std(Bstates[:,ind_ttrans])
            # Last Thread time
            std_tlastthread = np.std(Bstates[:,ind_tlastthread])
            # Exit time and count
            std_texit = np.std(Bstates[:,ind_texit])
            std_nexit = np.std(Bstates[:,ind_nexit])
            # Fill time
            std_tfill = np.std(Bstates[:,ind_tfill])
            # Stuck time and count, tail count
            std_tstuck = np.std(Bstates[:,ind_tstuck])
            var_nstuck = np.average((Bstates[:,ind_nstuck]-mean_nstuck)**2,weights=Bstates[:,ind_tstuck])
            var_ntail = np.average((Bstates[:,ind_nstuck]-mean_ntail)**2,weights=Bstates[:,ind_tstuck])
            std_nstuck = np.sqrt(var_nstuck)
            std_ntail = np.sqrt(var_ntail)
            # Average time to fail for failed events (already partially averaged)
            var_tfail = np.average((Bfails[:,ind_tfail]-mean_tfail)**2,weights=Bfails[:,ind_nevents])
            std_tfail = np.sqrt(var_tfail)

            # Save to matrix
            rcis = Bstates[0,ind_rcis]
            rtrans = Bstates[0,ind_rtrans]
            Output = np.vstack([Output,
                               [N,rcis,rcavi,rtrans,flux,
                                mean_ttrans,std_ttrans,
                                mean_tlastthread,std_tlastthread,
                                mean_texit,std_texit,
                                mean_nexit,std_nexit,
                                mean_tfill,std_tfill,
                                mean_tstuck,std_tstuck,
                                mean_nstuck,std_nstuck,
                                mean_ntail,std_ntail,
                                failrate,
                                mean_tfail,std_tfail]])
            i=i+1


        

np.savetxt('avg_metrics.dat',Output)



