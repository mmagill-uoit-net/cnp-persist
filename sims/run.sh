#!/bin/bash


# Executable and input script
myespresso='/home/magill/espresso-2.1.2j-harmonic-brown/Espresso'
myinputscript='cnp-persist.tcl'

# Constant parameters
rseed_suffix=123456
reff_cis=1.
reff_trans=1.
vis_flag=0
nevents=10

# Make log directory if it isn't already theree
mkdir -p data
mkdir -p logs

# Should tailor time limit to each set of cases... but not for now
time=5000m

for rseed_prefix in {1..5}
do

    for N in 30 60 90 120 150
    do

	for flux in 0.4 0.5 0.6 0.7 0.8
	do

	    for reff_cavi in 5. 10. 15. 20.
	    do

		rseed=${N}${rseed_prefix}${rseed_suffix}
		label=${reff_cavi}_${flux}_${N}_${rseed}
		sqsub -q serial -o logs/log_${label}.log -r $time $myespresso $myinputscript $N $reff_cis $reff_cavi $reff_trans $flux $vis_flag $nevents $rseed

	    done 
	done
    done
done






