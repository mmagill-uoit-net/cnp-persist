#!/bin/bash

for file in data/*_states*
do
	./process-states.exe $file
done

echo "States finished"

for file in data/*_fail*
do
	./process-fails.exe $file
done

echo "Fails finished"

python process-metrics.py
