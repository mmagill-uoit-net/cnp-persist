import numpy as np
import matplotlib.pyplot as plt


# Assumes bond length of 1, which is slightly wrong
# Should be closer to 0.97 or so

# Values
N = 20
#Ks = np.array([1,5,10,20,50,90],dtype=np.float)
Ks = np.linspace(1,300,100)
Lc = N-1

# # Measured
# LE2_means = np.zeros((len(Ks),))
# for i in range(len(Ks)):
#     K = Ks[i]
#     LE2_series = np.loadtxt('data/20_%d_1_123456789_LE2.dat'%K)
#     LE2_means[i] = np.mean(LE2_series)


# Theoretical
LE2_theoA = np.zeros((len(Ks),))
LE2_theoB = np.zeros((len(Ks),))
thetas = np.linspace(0,np.pi,1000)
for i in range(len(Ks)):
    K = Ks[i]

    # Hypothesis A
    Lp = K
    LE2_theoA[i] = 2*Lp * (Lc + 
                           Lp * (np.exp(-Lc/Lp) - 1)) 

    # Hypothesis B
    Etheta = ( np.sum( thetas * np.exp(-(K/2)*thetas**2) * np.sin(thetas) ) /
               np.sum(          np.exp(-(K/2)*thetas**2) * np.sin(thetas) ) )
    Lp = 2. / Etheta**2
    LE2_theoB[i] = 2*Lp * (Lc + 
                           Lp * (np.exp(-Lc/Lp) - 1)) 


#plt.loglog(Ks,LE2_means)
#plt.loglog(Ks,LE2_theoA,label="Lp=K")
#plt.loglog(Ks,LE2_theoB,label="Boltzmann")
plt.plot(Ks,LE2_theoA,label="Lp=K")
plt.plot(Ks,LE2_theoB,label="Boltzmann")
plt.xlabel('K')
plt.ylabel('LE2')
plt.legend(loc='upper left')
plt.show()
