# Example input:
# ~/espresso-2.1.2j/Espresso free-persist.tcl 20 10 1 1 1234

# Parse input (currently cis-trans symmetric and wall thickness of 1 sig)
set N            [lindex $argv 0]
set K            [lindex $argv 1]
set vis_flag     [lindex $argv 2]
set nevents      [lindex $argv 3]
set rseed        [lindex $argv 4]
t_random seed    $rseed

########################################################################################

# Function runs a single translocation
global VMD_IS_ON 
set VMD_IS_ON 0
proc RunMain { N K vis_flag Fout } {
    global VMD_IS_ON 

    # Scales
    set sig 1.0
    set eps 1.0

    # Domain size
    set box_l 400

    # Thermostat Parameters
    set temp 1.
    set gamma 1.

    # FENE Potential Parameters
    set kap [expr {30.0*$eps/($sig*$sig)}]
    set lam [expr {1.5*$sig}]

    # LJ Potential Parameters
    set cut   [expr {pow(2.0,1.0/6.0)*$sig}]
    set shift [expr {0.25*$eps}]

    ##########################################################################


    # Spatial domain creation
    setmd box_l $box_l $box_l $box_l

    # Temporal domain creation
    setmd time_step 0.01
    setmd skin 0.4

    # Interaction creations
    inter 0 fene $kap $lam
    inter 7 angle $K
    inter 0 0  lennard-jones $eps $sig $cut $shift 0.
    inter 0 76 lennard-jones $eps $sig $cut $shift 0.

    # Polymer creation (ncavi at time 0 is threads)
    # Need to offset from absolute center because potential is ill-defined (Espresso Bug)
    set threads 2
    set x [expr {$box_l/2. + 0.01}]
    set y [expr {$box_l/2. + 0.01}]
    set z [expr {$box_l/2. + 0.01}]
    part 0 pos $x $y $z type 0 fix 0 0 0 ext_force 0. 0. 0.
    # Build the chain (without angular potential)
    for { set i 1 } { $i < $N } { incr i } {
	# Z position
	set z [expr {$z - $sig}]
	# Place the ith particle
	part $i pos $x $y $z type 0 fix 0 0 0 ext_force 0. 0. 0.
	# FENE bond to the previous particle in the chain
	part $i bond 0 [expr {$i - 1}] 
    }
    # After all particles are placed, apply the angular potential (if any)
    for { set i 1 } { $i < [expr {$N-1}] } { incr i } {
	part $i bond 7 [expr {$i - 1}] [expr {$i + 1}]
    }


    ##########################################################################


    # Initialize Visualization
    if { $vis_flag == 1 } {
	if { $VMD_IS_ON == 0 } {
	    prepare_vmd_connection vmdout
	    imd listen 100
	    set VMD_IS_ON 1
	}
	imd positions
    }

    # Equilibrate (fix all threaded monomers)
    puts "Equilibrating..."
    thermostat langevin $temp 0.1
    for {set i 1} {$i < 1e4} {incr i 1} {
	integrate 100
	if { $vis_flag == 1 } {
	    imd positions
	}
    }
    puts "Equilibrated"

    # Simulate
    thermostat langevin $temp $gamma
    for {set timestep 0} {$timestep < 1e6} {incr timestep 1} {

	# Calculate L_E
	set LE [veclen [vecsub [part 0 print pos] [part [expr {$N-1}] print pos]]]
	set LE2 [expr {$LE*$LE}]
	puts $Fout  "$LE2"

	# Integrate (thus field only updates every 100 timesteps)
	integrate 100
	incr timestep 100
	if { $vis_flag == 1 } {
	    imd positions
	}
    }

    return 0
}


##########################################################################
##########################################################################

# Actually run the program

# Output files (common to all events)
set CaseName "${N}_${K}_${nevents}_${rseed}"
set Fout [open "data/${CaseName}_LE2.dat" "a+"]

# Run nevents translocations
for { set casenum 1 } { $casenum <= $nevents } { incr casenum } {

    # Run case
    set errcode 1
    while { $errcode == 1 } {
	set errcode [RunMain $N $K $vis_flag $Fout]

	# Unbond the polymer (so the function can be called again)
	for { set i 1 } { $i < $N } { incr i } {
	    part $i bond delete
	}   

	# Delete the constraints
	constraint delete
    }

    # Print newline to Fstate to separate arrays
    # Ffail doesn't need this
    puts $Fout ""
}

close $Fout

